package com.daioware.net.http.client.test;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;

import com.daioware.net.http.HttpResponse;
import com.daioware.net.http.client.HandlingException;
import com.daioware.net.http.client.HttpSender;
import com.daioware.net.http.client.ResponseBodyException;
import com.daioware.net.http.items.HttpHeader;
import com.daioware.net.http.items.QueryParameterList;
import com.daioware.net.http.request.HttpPostRequest;

public class testHttpPostSender {

	public static void main(String[] args) throws UnknownHostException, IOException, ResponseBodyException, HandlingException, URISyntaxException {
		String url="http://localhost:8080/storeTracker/session/logIn";
		HttpPostRequest request=new HttpPostRequest(url);
		QueryParameterList parametersList=new QueryParameterList();
		//request.addHeader("Accept-Encoding","utf-8");
		//request.addHeader("Connection","close");
		request.addHeader("Content-Type","application/x-www-form-urlencoded;charset=utf-8;");
		request.addHeader("User-Agent","Diego");
		request.addHeader("Accept","application/json");
		request.addHeader("Connection","close");
		parametersList.add("email","diego6569olvera@gmail.com");
		parametersList.add("password","password");

		request.setBodyStr(parametersList.toString(request.getEncoding()));
		HttpSender sender=new HttpSender(request);
		sender.setDownloadSpeedInBytes(512);
		
		System.out.println("REQUEST");
		System.out.println("Port:"+request.getPort());
		System.out.println(request.getMetadataAsString());
		System.out.println("Body\n"+request.getBodyStr());
		System.out.println("-------");
		HttpResponse resp=sender.send(0);
		assertNotNull(resp);
		System.out.println("Response");
		System.out.println("Headers:");
		for(HttpHeader header:resp.getHeaders().values()) {
			System.out.println(header);
		}		
		System.out.println("Other data:");
		System.out.println("status:"+resp.getStatus());
		System.out.println("version:"+resp.getVersion());
		System.out.println("status message:"+resp.getStatusMessage());
		//System.out.println("Body:\n"+resp.getBodyAsString());
		
		System.out.println("f:"+resp.getBodyAsFile().getAbsolutePath());
	}
}
