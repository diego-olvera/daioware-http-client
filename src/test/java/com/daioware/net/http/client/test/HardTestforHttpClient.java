package com.daioware.net.http.client.test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.daioware.net.http.HttpResponse;
import com.daioware.net.http.client.FileHandler;
import com.daioware.net.http.client.HttpSender;
import com.daioware.net.http.items.HttpHeader;
import com.daioware.net.http.request.HttpGetRequest;
import com.daioware.net.http.request.HttpRequest;

public class HardTestforHttpClient {

	public static void main(String[] args) throws Exception{
		String url="http://79.127.126.110/Serial/The%20Big%20Bang%20Theory/S01/";
		//url="https://bitbucket.org/diego-olvera/daioware.com-http-website-copier/src/master/";
		//url="https://www.httrack.com";
		//url="http://www.tarjetarojatv.online/";
		url="http://localhost/storeTracker/login.html";
		url="http://localhost/storeTracker/";
		url="https://www.w3schools.com/default.asp";
		HttpRequest request=new HttpGetRequest(url);
		request.addHeader("Connection","close");
		HttpSender sender=new HttpSender(request);
		sender.setRespHandler(new FileHandler());
		sender.setDownloadSpeedInBytes(5);
		sender.setOutputFolder(new File("./download/"));
		System.out.println("Sending");
		HttpResponse resp=sender.send();
		System.out.println("Sent");
		String s=new String(Files.readAllBytes(Paths.get(resp.getBodyAsFile().getAbsolutePath())));
		System.out.println("Reading");
		System.out.println("S:"+s);
		for(HttpHeader header:resp.getHeaders().values()){
			System.out.println(header);
		}
	}
}
