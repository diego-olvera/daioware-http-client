package com.daioware.net.http.client.test;

import static org.junit.Assert.assertEquals;

import com.daioware.commons.wrapper.WrapperString;
import com.daioware.net.http.HttpUtil;

public class TestContentTypeParser {

	public static void main(String[] args) {
		WrapperString mimeType = new WrapperString();
		WrapperString charset = new WrapperString();
		String headerValue="application/x-www-form-urlencoded;charset=utf-8;";
		HttpUtil.parseContentType(mimeType, charset,headerValue);
		
		assertEquals("application/x-www-form-urlencoded", mimeType.value);
		assertEquals("utf-8",charset.value);
	}
}
