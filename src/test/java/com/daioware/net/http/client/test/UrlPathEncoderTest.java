package com.daioware.net.http.client.test;

import java.net.MalformedURLException;

import org.junit.Test;

import com.daioware.net.http.UrlPathEncoder;

import junit.framework.TestCase;

public class UrlPathEncoderTest extends TestCase{

	@Test
	public void test() throws MalformedURLException {
		String url="http://localhost";
		String url2="http://localhost/ la biblia del java";
		
		assertFalse(UrlPathEncoder.needsEncoding(url));
		assertTrue(UrlPathEncoder.needsEncoding(url2));

	}
}
