package com.daioware.net.http.client.test;


import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

import com.daioware.net.http.UrlPathEncoder;
import com.daioware.net.http.request.HttpGetRequest;
import com.daioware.net.http.request.HttpRequest;

public class testHttpRequest {

	public static void main(String[] args) throws MalformedURLException, UnsupportedEncodingException, URISyntaxException {
		HttpRequest request=new HttpGetRequest(UrlPathEncoder.encode("http://localhost/La Biblia del Java.pdf"));
		request.addHeader("Accept-Encoding","text/html");
		request.addHeader("User-Agent","Diego");
		request.addQueryParam("param1","val1 je je |");
		request.addQueryParam("param2","val2");
		request.addHeader("Connection","close");

		String reqString;
		try {
			reqString=request.getMetadataAsString();
			System.out.println(reqString);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}
