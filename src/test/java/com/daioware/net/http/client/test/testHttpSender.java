package com.daioware.net.http.client.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;

import com.daioware.commons.ProgressNotifier;
import com.daioware.net.http.HttpResponse;
import com.daioware.net.http.UrlPathEncoder;
import com.daioware.net.http.client.FileHandler;
import com.daioware.net.http.client.HandlingException;
import com.daioware.net.http.client.HttpSender;
import com.daioware.net.http.client.ResponseBodyException;
import com.daioware.net.http.items.HttpHeader;
import com.daioware.net.http.request.HttpGetRequest;
import com.daioware.net.http.request.HttpRequest;

class SystemProgressNotifer extends ProgressNotifier{

	public SystemProgressNotifer(float currentProgress, Float totalProgress) {
		super(currentProgress, totalProgress);
	}

	public SystemProgressNotifer(Float totalProgress) {
		super(totalProgress);
	}

	@Override
	public void setCurrentProgress(float currentProgress) {
		super.setCurrentProgress(currentProgress);
		Float f=getPercentageCompleted();
		if(f!=null) {
			System.out.printf("Current %% downloaded %.2f\n",f);
		}
	}
	
}
public class testHttpSender {

	public static void main(String[] args) throws UnknownHostException, IOException, ResponseBodyException, HandlingException, URISyntaxException {
		String url="http://localhost/storeTracker/login.html";
		url="http://localhost/La Biblia del Java.pdf";
		url=UrlPathEncoder.encode(url);
		HttpRequest request=new HttpGetRequest(url);
		request.addHeader("Accept-Encoding","utf-8");
		request.addHeader("Content-Type","text/html;charset=UTF-8");
		request.addHeader("User-Agent","Diego");
		/*request.addQueryParam("param1","val1");
		request.addQueryParam("param2","val2");
*/
		HttpSender sender=new HttpSender(request);
		//sender.setProgressNotifier(new SystemProgressNotifer(null));
		sender.setDownloadSpeedInBytes(512*4);
		sender.setRespHandler(new FileHandler());
		System.out.println("REQUEST");
		System.out.println(request.getMetadataAsString());
		System.out.println("-------");
		HttpResponse resp=sender.send();
		assertNotNull(resp);
		System.out.println("Response");
		System.out.println("Headers:");
		for(HttpHeader header:resp.getHeaders().values()) {
			System.out.println(header);
		}		
		System.out.println("Other data:");
		System.out.println("status:"+resp.getStatus());
		System.out.println("version:"+resp.getVersion());
		System.out.println("status message:"+resp.getStatusMessage());
		//System.out.println("Body:\n"+resp.getBodyAsString());
		
		File file;
		//file=resp.getBodyAsFile(new File("otro archivo.pdf"));
		file=resp.getBodyAsFile();
		System.out.println("f:"+file.getAbsolutePath());
		assertTrue(file.exists());

	}
}
