package com.daioware.net.http.client.test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.URISyntaxException;

import org.junit.Test;

import com.daioware.net.http.HttpResponse;
import com.daioware.net.http.client.HandlingException;
import com.daioware.net.http.client.HttpSender;
import com.daioware.net.http.client.ResponseBodyException;
import com.daioware.net.http.items.HttpHeader;
import com.daioware.net.http.request.HttpGetRequest;
import com.daioware.net.http.request.HttpRequest;

import junit.framework.TestCase;

public class TestProxy extends TestCase{

	@Test
	public void test() throws URISyntaxException, UnsupportedEncodingException, IOException, HandlingException, ResponseBodyException {
		SocketAddress addr=new InetSocketAddress("103.228.118.14",44577);
		Proxy proxy=new Proxy(Proxy.Type.HTTP, addr);
		//proxy=Proxy.NO_PROXY;
		HttpRequest request=new HttpGetRequest("http://localhost/dashboard/");
		//request.addHeader("Accept-Encoding","utf-8");
		request.addHeader("Content-Type","text/html;charset=UTF-8");
		System.out.println(request.getMetadataAsString());
		HttpSender sender=new HttpSender(request);
		HttpResponse response=sender.send(proxy);
		for(HttpHeader header:response.getHeaders().values()) {
			System.out.println(header);
		}
		System.out.println(response.getBodyAsString());
	}
}
