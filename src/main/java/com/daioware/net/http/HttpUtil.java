package com.daioware.net.http;

import java.util.ArrayList;
import java.util.List;

import com.daioware.commons.string.Regex;
import com.daioware.commons.wrapper.WrapperString;

public class HttpUtil {

	public static final String DEFAULT_ENCODING="utf-8";
	
	private static List<Regex> plainTextMimeTypes=new ArrayList<>(5);
	private static List<Regex> fileMimeTypes=new ArrayList<>(2);

	static {
		plainTextMimeTypes.add(new Regex("^text/[\\S]+"));
		plainTextMimeTypes.add(new Regex("application/json"));
		plainTextMimeTypes.add(new Regex("application/javascript"));
		plainTextMimeTypes.add(new Regex("application/xml"));
		plainTextMimeTypes.add(new Regex("application/ecmascript"));

		fileMimeTypes.add(new Regex("^audio/[\\S]+"));
		fileMimeTypes.add(new Regex("^video/[\\S]+"));
		fileMimeTypes.add(new Regex("^image/[\\S]+"));
		
	}
	
	public static void parseContentType(WrapperString mimeType,WrapperString charset,String headerValue) {
		int s="charset=".length();
		int index=headerValue.indexOf("charset=");
		charset.value=index!=-1?headerValue.substring(index+s):HttpUtil.DEFAULT_ENCODING;
		mimeType.value=index>=0?headerValue.substring(0, index-1):headerValue;
		charset.value=charset.value.replaceAll(";","");
	}

	public static boolean isValid(List<Regex> regexes,String mimeToCompare) {
		for(Regex currentMime:regexes) {
			if(currentMime.matches(mimeToCompare)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isPlainTextMimeType(String mime) {
		boolean b=isValid(plainTextMimeTypes,mime);
		return !b?!isValid(fileMimeTypes,mime):b;
	}	
}
