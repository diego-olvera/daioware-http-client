package com.daioware.net.http;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

import com.daioware.commons.wrapper.WrapperString;
import com.daioware.file.MyRandomAccessFile;
import com.daioware.net.http.client.HttpSender;
import com.daioware.net.http.client.ResponseBodyException;
import com.daioware.net.http.items.HttpHeader;
import com.daioware.net.http.request.HttpRequest;

public class HttpResponse {

	private HttpRequest request;
	private Map<String,HttpHeader> headers;
	private List<Byte> body;
	private String version;
	private Integer status;
	private String statusMessage;
	private File file;
	private HttpSender sender;
	
	public HttpResponse(Map<String, HttpHeader> headers, List<Byte> body, String version,
			Integer status, String statusMessage,HttpRequest request,HttpSender sender) {
		this.headers = headers;
		this.body = body;
		this.version = version;
		this.status = status;
		this.statusMessage = statusMessage;
		this.request=request;
		this.sender=sender;
	}

	public Map<String, HttpHeader> getHeaders() {
		return headers;
	}

	public List<Byte> getBody() {
		return body;
	}

	public String getVersion() {
		return version;
	}

	public Integer getStatus() {
		return status;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	protected void setHeaders(Map<String, HttpHeader> headers) {
		this.headers = headers;
	}

	protected void setBody(List<Byte> body) {
		this.body = body;
	}

	protected void setVersion(String version) {
		this.version = version;
	}

	protected void setStatus(Integer status) {
		this.status = status;
	}

	protected void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	
	public HttpRequest getRequest() {
		return request;
	}

	public String getBodyAsString() throws ResponseBodyException {
		HttpHeader header=headers.get("Content-Type");
		WrapperString mime,charsetWrapper;
		Charset wrapper;
		if(header!=null) {
			mime=new WrapperString();
			charsetWrapper=new WrapperString();
			HttpUtil.parseContentType(mime,charsetWrapper,header.getValue());
			if(!HttpUtil.isPlainTextMimeType(mime.value)) {
				throw new ResponseBodyException("Mime type '"+mime.value+"' is not  valid for plain text");
			}
			wrapper=Charset.forName(charsetWrapper.value);
		}
		else {
			wrapper=Charset.forName(HttpUtil.DEFAULT_ENCODING);
		}
		return getBodyAsString(wrapper);
	}
	
	public String getBodyAsString(Charset charset) {
		byte bytes[]=new byte[body.size()];
		int i=0;
		for(byte b:body) {
			bytes[i++]=b;
		}
		return new String(bytes,0,bytes.length,charset);
	}
	
	public File getBodyAsFile(File file) throws IOException {
		if(this.file!=null) {//let's rename the file that has been downloaded previously
			if(this.file.renameTo(file)) {
				this.file=file;
			}
		}
		else {
			this.file=file;
			try(MyRandomAccessFile raFile=new MyRandomAccessFile(file, "rw")) {
				raFile.setLength(0);
				byte bytes[]=new byte[body.size()];
				int s=0;
				for(byte b:body) {
					bytes[s++]=b;
				}
				raFile.write(bytes);
			}
		}
		return this.file;
		
	}
	
	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public File getBodyAsFile() throws IOException {
		File file=getFile();
		if(file==null) {
			return getBodyAsFile(sender.getOutputFile());
		}
		//else file already downloaded while downloading the bytes in HttpSender
		return file;
	}
}
