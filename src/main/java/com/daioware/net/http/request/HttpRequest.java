package com.daioware.net.http.request;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.daioware.net.http.HttpUtil;
import com.daioware.net.http.items.HttpHeader;
import com.daioware.net.http.items.QueryParameter;
import com.daioware.net.http.items.QueryParameterList;

public abstract class HttpRequest {

	private static Map<String,Integer> ports=new HashMap<>(2);
	
	static {
		ports.put("http",80);
		ports.put("https", 443);
	}
	
	private URI uri;
	private String encoding;
	private List<HttpHeader> headers;
	private QueryParameterList paramList;
	
	private HttpRequest() {
		headers=new ArrayList<>();
		paramList=new QueryParameterList();
	}
	
	public HttpRequest(URI uri,String encoding) throws URISyntaxException {
		this();
		setUri(uri);
		setEncoding(encoding);
	}
	
	public HttpRequest(URI url) throws URISyntaxException {
		this(url,HttpUtil.DEFAULT_ENCODING);
	}
	
	public HttpRequest(String url,String encoding)throws URISyntaxException {
		this(new URI(url),encoding);
	}
	
	public URI getUri() {
		return uri;
	}

	public void setUri(URI uri) {
		this.uri = uri;
	}

	public String getHost() {
		return getUri().getHost();
	}

	public String getPath() {
		return getUri().getPath();
	}

	public abstract String getMethod();
	
	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public List<HttpHeader> getHeaders() {
		return headers;
	}

	public void setHeaders(List<HttpHeader> headers) {
		this.headers = headers;
	}

	public List<QueryParameter> getQueryParameters() {
		return paramList.getParams();
	}

	public void setQueryParameters(List<QueryParameter> queryParameters) {
		paramList.setParams(queryParameters);
	}

	public String toStringQueryParameters() throws UnsupportedEncodingException {
		return paramList.toString(getEncoding());
	}
	
	public String toStringHeaders() {
		StringBuilder headersStringBuilder=new StringBuilder();
		headersStringBuilder.append("host:").append(getHost()).append("\r\n");
		headersStringBuilder.append("Content-Length:").append(getContentLength()).append("\r\n");
		for(HttpHeader parameter:headers) {
			headersStringBuilder.append(parameter.toString()).append("\r\n");
		}
		return headersStringBuilder.toString();
	}
		
	public abstract long getContentLength();

	public String getMetadataAsString() throws UnsupportedEncodingException {
		return new StringBuilder()
			.append(getMethod()).append(" ").append(getUri().getRawPath())
			.append(paramList.size()>=1?"?":"")
			.append(toStringQueryParameters()).append(" HTTP/1.1").append("\r\n")
			.append(toStringHeaders())
			.append("\r\n")
			.toString();
	}

	public boolean addHeader(String key, String value) {
		return addHeader(new HttpHeader(key, value));
	}
	
	public boolean addHeader(HttpHeader header) {
		return headers.add(header);
	}
	
	public abstract boolean startReadingBody()throws IOException;
	public abstract int readBody(byte[] bytes,int offset,int length)throws IOException;
	public abstract void stopReadingBody()throws IOException;
	
	public boolean addQueryParam(String key, String value) {
		return addQueryParam(new QueryParameter(key, value));
	}
	
	public boolean addQueryParam(QueryParameter q) {
		return paramList.add(q);
	}

	public String getFileName() throws UnsupportedEncodingException {
		String path=getPath();
		int length=path.length(),index;
		int slashesCounted;
		//remove "/" at the end
		slashesCounted=countSlashes(path);
		if(slashesCounted>=1) {//Not counting the https://
			if(path.length()>=1 && path.charAt(length-1)=='/'){
				path=path.substring(0, length-1);
			}
			index=path.lastIndexOf("/");
			path=path.substring(index+1);
		}
		else {
			path=getHost();
		}
		return path;
	}
	public static int countSlashes(String path) {
		int counter=0,leftIndex=0,rightIndex;
		while((rightIndex=path.indexOf("/",leftIndex))>=0) {
			counter++;
			leftIndex=rightIndex+1;
		}
		return counter;
	}
	public int getPort() {
		URI url=getUri();
		int port=url.getPort();
		return port!=-1?port:getPort(url.getScheme());
	}
	
	public static int getPort(String protocol) {
		Integer port=ports.get(protocol);
		return port!=null?port:80;
	}

}
