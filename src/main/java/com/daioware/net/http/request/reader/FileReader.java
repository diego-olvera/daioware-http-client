package com.daioware.net.http.request.reader;

import java.io.File;
import java.io.IOException;

import com.daioware.file.MyRandomAccessFile;

public class FileReader implements Reader {

	private File file;
	private MyRandomAccessFile raFile;

	public FileReader(File file) {
		this.file = file;
	}

	@Override
	public boolean canOpen() {
		return file.exists();
	}

	@Override
	public void open() throws IOException {
		raFile = new MyRandomAccessFile(file, "r");
	}

	@Override
	public int read(byte[] bytes, int offset, int length) throws IOException {
		return raFile.read(bytes, offset, length);
	}

	@Override
	public void close() throws IOException {
		raFile.close();
	}

	@Override
	public long size() {
		return file.length();
	}

}
