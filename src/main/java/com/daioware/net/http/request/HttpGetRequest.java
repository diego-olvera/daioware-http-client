package com.daioware.net.http.request;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class HttpGetRequest extends HttpRequest{

	public HttpGetRequest(URI uri, String encoding) throws URISyntaxException {
		super(uri, encoding);
	}

	public HttpGetRequest(URI url) throws URISyntaxException {
		super(url);
	}

	public HttpGetRequest(String url) throws URISyntaxException {
		super(new URI(url));
	}
	
	@Override
	public int readBody(byte[] bytes, int offset, int length) {
		return 0;
	}

	@Override
	public boolean startReadingBody() throws IOException {
		return false;
	}

	@Override
	public void stopReadingBody() throws IOException {
		
	}

	@Override
	public long getContentLength() {
		return 0;
	}

	@Override
	public String getMethod() {
		return "GET";
	}
	
}
