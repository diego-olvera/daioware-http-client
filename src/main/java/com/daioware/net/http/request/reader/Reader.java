package com.daioware.net.http.request.reader;

import java.io.IOException;

public interface Reader{
	public boolean canOpen();
	public long size();
	public void open()throws IOException;
	public int read(byte[] bytes, int offset, int length)throws IOException;
	public void close()throws IOException;
}

