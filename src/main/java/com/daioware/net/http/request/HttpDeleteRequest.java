package com.daioware.net.http.request;

import java.net.URI;
import java.net.URISyntaxException;

public class HttpDeleteRequest extends HttpGetRequest{

	public HttpDeleteRequest(URI uri, String encoding) throws URISyntaxException {
		super(uri, encoding);
	}

	public HttpDeleteRequest(URI url) throws URISyntaxException {
		super(url);
	}
	
	public HttpDeleteRequest(String url) throws URISyntaxException {
		super(url);
	}


	@Override
	public String getMethod() {
		return "DELETE";
	}
	
}
