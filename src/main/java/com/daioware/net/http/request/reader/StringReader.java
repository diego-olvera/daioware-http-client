package com.daioware.net.http.request.reader;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class StringReader implements Reader{
	
	private byte[] stringBytes;
	private int currentIndex;
	
	public StringReader(String string,String charset) throws UnsupportedEncodingException {
		this(string.getBytes(charset),0);
	}
	public StringReader(byte[] stringBytes,int currentIndex) {
		this.stringBytes=stringBytes;
		this.currentIndex=currentIndex;
	}
	
	@Override
	public int read(byte[] bytes, int offset, int length)throws IOException {
		int bytesRead,j;
		for(bytesRead=0,j=stringBytes.length;offset<length && currentIndex<j;offset++) {
			bytes[offset]=stringBytes[currentIndex++];
			bytesRead++;
		}
		return bytesRead;
	}
	@Override
	public boolean canOpen() {
		return stringBytes.length>=1;
	}
	@Override
	public void open() throws IOException {
		currentIndex=0;
	}
	@Override
	public void close() throws IOException {
		currentIndex=0;
	}

	@Override
	public long size() {
		return stringBytes.length;
	}
	
}
