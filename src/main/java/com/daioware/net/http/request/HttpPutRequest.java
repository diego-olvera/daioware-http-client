package com.daioware.net.http.request;

import java.net.URI;
import java.net.URISyntaxException;

public class HttpPutRequest extends HttpPostRequest{

	
	public HttpPutRequest(String url, String encoding) throws URISyntaxException {
		super(url, encoding);
	}

	public HttpPutRequest(URI uri, String encoding) throws URISyntaxException {
		super(uri, encoding);
	}

	public HttpPutRequest(URI url) throws URISyntaxException {
		super(url);
	}

	@Override
	public String getMethod() {
		return "PUT";
	}
}
