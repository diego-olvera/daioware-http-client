package com.daioware.net.http.items;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class QueryParameterList {

	private List<QueryParameter> params;
	
	public QueryParameterList() {
		this(new ArrayList<>());
	}
	
	public QueryParameterList(int initialSize) {
		this(new ArrayList<>(initialSize));
	}
	
	public QueryParameterList(List<QueryParameter> params) {
		this.params=params;
	}
	
	public List<QueryParameter> getParams() {
		return params;
	}

	public void setParams(List<QueryParameter> params) {
		this.params = params;
	}

	public boolean add(String key,String value) {
		return add(new QueryParameter(key, value));
	}
	
	public boolean add(QueryParameter q) {
		return params.add(q);
	}
	
	public QueryParameter addOrReplace(QueryParameter parameter) {
		int index=params.indexOf(parameter);
		if(index>=0) {
			return params.set(index, parameter);
		}
		add(parameter);
		return null;
	}
	
	public QueryParameter addOrReplace(String key,String value) {
		return addOrReplace(new QueryParameter(key, value));
	}
	
	public String toString(String encoding) throws UnsupportedEncodingException {
		StringBuilder info=new StringBuilder();
		String sep="";
		for(QueryParameter q:params) {
			info.append(sep).append(q.toString(encoding));
			sep="&";
		}
		return info.toString();
	}
	
	public QueryParameter remove(String name) {
		ListIterator<QueryParameter> iterator=params.listIterator();
		QueryParameter param;
		while(iterator.hasNext()) {
			param=iterator.next();
			if(param.getKey().equals(name)) {
				iterator.remove();
				return param;
			}
		}
		return null;
	}
	
	public QueryParameter get(String name) {
		for(QueryParameter q:params) {
			if(q.getKey().equals(name)) {
				return q;
			}
		}
		return null;
	}
	
	public void loadEncodedParameters(String queryLine,String encoding) throws UnsupportedEncodingException {
		String nameAndValuePair[];
		for(String nameAndValue:queryLine.split("&")) {
			nameAndValuePair=nameAndValue.split("=");
			add(nameAndValuePair[0],URLDecoder.decode(nameAndValuePair[1],encoding));
		}
	}

	public int size() {
		return params.size();
	}
}
