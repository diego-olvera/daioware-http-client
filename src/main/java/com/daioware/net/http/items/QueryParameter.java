package com.daioware.net.http.items;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class QueryParameter extends KeyValue{

	public QueryParameter(String key, String value) {
		super(key,value);
	}

	public String toString(String encoding) throws UnsupportedEncodingException {
		String val=getValue();
		return val!=null?getKey()+"="+URLEncoder.encode(val, encoding):"";
	}
}
