package com.daioware.net.http.items;

import java.io.UnsupportedEncodingException;

public class HttpHeader extends KeyValue{

	public HttpHeader(String key, String value) {
		super(key, value);
	}

	public String toString(String encoding) throws UnsupportedEncodingException {
		return toString();
	}
	public String toString() {
		return value!=null?getKey()+": "+getValue():"";
	}
	
}
