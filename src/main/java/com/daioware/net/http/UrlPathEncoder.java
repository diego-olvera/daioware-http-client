package com.daioware.net.http;

import java.net.MalformedURLException;
import java.net.URL;

public class UrlPathEncoder {
	private static char toHex(int ch) {
        return (char) (ch < 10 ? '0' + ch : 'A' + ch - 10);
    }

    private static boolean isUnsafe(char ch) {
        return (ch > 128 || ch < 0)?true:" %$&,/:;=?@<>#%".indexOf(ch) >= 0;
    }
    
    public static boolean needsEncoding(String url) throws MalformedURLException {
    	String path=new URL(url).getPath();
    	for(int i=0,j=path.length();i<j;i++) {
    		if(isUnsafe(path.charAt(i))) {
    			return true;
    		}
    	}
    	return false;
    }
    
    public static String encodeIfNeccessary(String url) throws MalformedURLException {
    	return needsEncoding(url)?encode(url):url;
    }
    
	public static String encode(String url) throws MalformedURLException {
    	StringBuilder urlBuilder=new StringBuilder();
    	URL urlObject=new URL(url);
		String fullPath=urlObject.getPath();
		String paths[]=fullPath.split("/");
		int i,pathsSize=paths.length,port=urlObject.getPort();
		urlBuilder.append(urlObject.getProtocol()).append("://").append(urlObject.getHost());
		if(port!=-1) {
			urlBuilder.append(":").append(port);
		}
		i=0;
		for(String pathAux:paths) {
			for (char ch : pathAux.toCharArray()) {
	            if (isUnsafe(ch)) {
	            	urlBuilder.append('%');
	            	urlBuilder.append(toHex(ch / 16));
	            	urlBuilder.append(toHex(ch % 16));
	            } else {
	            	urlBuilder.append(ch);
	            }
	        }
			if(i+1<pathsSize) {
				urlBuilder.append("/");
			}
			i++;
		}
		if(pathsSize>=1 && fullPath.charAt(fullPath.length()-1)=='/') {
			urlBuilder.append("/");
		}
		return urlBuilder.toString();        
    }
}
