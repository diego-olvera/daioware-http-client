package com.daioware.net.http.client;

import java.util.List;

import com.daioware.net.http.items.HttpHeader;

public class EmptyHandler implements ResponseHandler{

	public static final EmptyHandler DEFAULT_INSTANCE=new EmptyHandler();
	
	@Override
	public void open(HttpSender sender) throws HandlingException {
		
	}

	@Override
	public void close() throws HandlingException {
		
	}

	@Override
	public void handleHeaders(byte[] bytes, int offset, int length) throws HandlingException {
		
	}
	
	@Override
	public void handleBody(byte[] bytes, int offset, int length) throws HandlingException {
		
	}

	@Override
	public void setHeaders(List<HttpHeader> headers) {
		
	}

}
