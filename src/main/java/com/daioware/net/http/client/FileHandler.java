package com.daioware.net.http.client;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import com.daioware.file.MyRandomAccessFile;
import com.daioware.net.http.HttpResponse;
import com.daioware.net.http.items.HttpHeader;

public class FileHandler implements ResponseHandler{

	private File file;
	private HttpSender sender;
	private MyRandomAccessFile raFile;
	
	public void handleHeaders(byte[] bytes, int offset, int length)throws HandlingException {
		
	}
	@Override
	public void handleBody(byte[] bytes, int offset, int length)throws HandlingException {
		try{
			raFile.write(bytes, offset, length);
		}catch(IOException e) {
			throw new HandlingException(e);
		}
	}
	@Override
	public void open(HttpSender sender)throws HandlingException  {
		this.sender=sender;
		if(raFile!=null) {
			return;
		}
		try {
			file=sender.getOutputFile();
		} catch (UnsupportedEncodingException e1) {
			throw new HandlingException(e1);
		}
		try {
			raFile=new MyRandomAccessFile(file, "rw");
		}catch(IOException e) {
			throw new HandlingException(e);
		}
		
	}
	@Override
	public void close()throws HandlingException  {
		HttpResponse resp;
		if(raFile==null) {
			return;
		}
		try {
			raFile.close();
			raFile=null;
			resp=sender.getResponse();
			if(resp!=null) {
				resp.setFile(file);
			}
		} catch (IOException e) {
			throw new HandlingException(e);
		}
	}
	@Override
	public void setHeaders(List<HttpHeader> headers) {
		// TODO Auto-generated method stub
		
	}
}
