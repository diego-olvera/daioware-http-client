package com.daioware.net.http.client;

public class ResponseBodyException extends Exception{

	private static final long serialVersionUID = 1L;

	public ResponseBodyException() {
		super();
	}

	public ResponseBodyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ResponseBodyException(String message, Throwable cause) {
		super(message, cause);
	}

	public ResponseBodyException(String message) {
		super(message);
	}

	public ResponseBodyException(Throwable cause) {
		super(cause);
	}

	
}
