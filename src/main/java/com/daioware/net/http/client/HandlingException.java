package com.daioware.net.http.client;

public class HandlingException extends Exception{

	private static final long serialVersionUID = 1L;

	public HandlingException() {
		super();
	}

	public HandlingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public HandlingException(String message, Throwable cause) {
		super(message, cause);
	}

	public HandlingException(String message) {
		super(message);
	}

	public HandlingException(Throwable cause) {
		super(cause);
	}

	
}
