package com.daioware.net.http.client;

import java.util.List;

import com.daioware.net.http.items.HttpHeader;

public interface ResponseHandler {

	public void open(HttpSender sender)throws HandlingException ;
	
	public void close()throws HandlingException ;
	
	public void setHeaders(List<HttpHeader> headers);
	public void handleHeaders(byte[] bytes,int offset,int length)throws HandlingException ;
	public void handleBody(byte[] bytes,int offset,int length)throws HandlingException ;

}
